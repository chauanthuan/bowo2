<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $primaryKey = 'location_id';

    protected $table = 'locations';

    protected $fillable = [
        'address', 'lat', 'long','event_id'
    ];

    public function event(){
        return $this->belongsTo('App\Event');
    }
}
