<?php

namespace App;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    const ACCOUNT_TYPE_EMAIL    = 0;
    const ACCOUNT_TYPE_FACEBOOK = 1;
    const ACCOUNT_TYPE_GOOGLE   = 2;
    const ACCOUNT_TYPE_TWITTER  = 3;
    const ACCOUNT_TYPE_GITHUB   = 4;

    const APPROVED      = 1;
    const UNAPPROVED    = 0;

    const GENDERFEMALE  = 0;
    const GENDERMALE    = 1;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'social_id', 'account_type', 'name', 'email', 'password', 'lang', 'state','last_period','longtime_period','cycle_period','avatar'
    ];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
