<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    const PRIORITY_HIGH             = 3;
    const PRIORITY_MEDIUM           = 2;
    const PRIORITY_LOW              = 1;

    const REPEAT_NONE               = 0;
    const REPEAT_DAILY              = 1;
    const REPEAT_EVERY_WEEKDAY      = 2;
    const REPEAT_WEEKLY             = 3;
    const REPEAT_MONTHLY            = 4;
    const REPEAT_YEARLY             = 5;
    const REPEAT_CUSTOM             = 6;

    const REMINDER_NONE             = 0;
    const REMINDER_ONTIME           = 1;
    const REMINDER_5MINUTE_BF       = 2;
    const REMINDER_30MINUTE_BF      = 3;
    const REMINDER_1HOUR_BF         = 4;
    const REMINDER_1DAY_BF          = 5;
    const REMINDER_2DAY_BF          = 6;
    

    protected $table = 'events';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $primaryKey = 'event_id';

    protected $fillable = [
        'title','description','priority','all_day','reminder','repeat','start_date','start_time','end_date','end_time','calendar_id','user_id','state'
    ];

    public function eventTag(){
        return $this->belongsTo('App\EventTag','event_tag_id');
    }

    public function locations(){
        return $this->hasMany('App\Location','event_id');
    }

    public function images(){
        return $this->hasMany('App\Image','event_id');
    }
}
