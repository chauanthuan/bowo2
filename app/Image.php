<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $primaryKey = 'img_id';

    protected $table = 'images';

    protected $fillable = [
        'img_path','event_id'
    ];

    public function event(){
        return $this->belongsTo('App\Event');
    }
}
