<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Tag;
use App\EventTag;
use App\Location;
use App\Image;
use App\User;
use App\Event;
use App\EventUserInvite;
use DB;
use Log;
use Validator;
use Carbon\Carbon;

class EventController extends Controller
{
    public function getAll(Request $request){
        //ket qua chi lay thong tin cua event va tagname
        //state: 1 = complete or 0 = incomplete
        $state = $request->get('state', null);
        $sort = $request->get('sort',null); //defaul by time, sort by priority, sort by tag
        $from_date = $request->get('from_date');
        $to_date = $request->get('to_date');
        $query = \DB::table('events')->select('events.*');
        $only_date = $request->get('only_date', false);
        if($state){
            $query->where('state', $state);
        }
        if($from_date){
            if($to_date){
                $query->where('start_date','>=',$from_date)->where('start_date','<=',$to_date);
            }
            else{
               $query->where('start_date','<=',$from_date); 
            }   
        }
        $listEvent = $query->get();
    	$locations = [];
    	$images = [];
    	$calendar = [];
    	$tags = [];
        $inviter = [];
        $listOnlyDate = [];
    	if($listEvent){
    		foreach ($listEvent as $key => $event) {
                if (!in_array($event->start_date, $listOnlyDate)) {
                    $listOnlyDate[] = $event->start_date;
                }
                if(!$only_date){
                    $tags = DB::table('tags')
                    ->join('event_tag','event_tag.tag_id','=','tags.tag_id')
                    ->join('events','events.event_id','=','event_tag.event_id')
                    ->where('events.event_id',$event->event_id)->select('tags.tag_id', 'tags.tag_name', 'tags.tag_slug')->get();
                    $listEvent[$key]->tags = $tags->toArray();
                }
    			
    		}
            if(!$only_date){
                if($sort && $sort == 'priority'){
                    $listEvent = collect($listEvent)->sortByDesc(function ($obj, $key) {
                        return $obj->priority;
                    });
                }
                else if($sort && $sort == 'tag'){
                    $listEvent = collect($listEvent)->sortByDesc(function ($obj, $key) {
                        return $obj->tags->count();
                    });
                }
                //order by start time(default)
                else{
                    $listEvent = collect($listEvent)->sortBy(function ($obj, $key) {
                        return strtotime($obj->start_time);
                    });
                }

                $listEvent = $listEvent->groupBy('start_date');
            }
            else{
                $listEvent = $listOnlyDate;
            }
            

 			return response()->json(['listevent' => $listEvent,'stt'=> 1]);
    	}

    	return response()->json(['stt'=> 1, 'msg'=>'No data events']);
    }

    public function detailEvent(Request $request, $eventId){
        $event = Event::where('event_id',$eventId)->first();

        if(isset($event) && $event != null){
            //get tag by event id
            $tags = DB::table('tags')
                        ->join('event_tag','event_tag.tag_id','=','tags.tag_id')
                        ->join('events','events.event_id','=','event_tag.event_id')
                        ->where('events.event_id',$event->event_id)->select('tags.tag_id', 'tags.tag_name', 'tags.tag_slug')->get();

            $event['tags'] = $tags;

            $locations = $event->locations()->select('location_id', 'address', 'lat', 'long')->get();
            $event['locations'] = $locations;

            $images = $event->images()->select('img_id','img_path')->get();
            $event['images'] = $images;

            $inviter = DB::table('users')
                    ->join('event_user_invite as eui','eui.user_id','=','users.id')
                    ->join('events','events.event_id','=','eui.event_id')
                    ->where('events.event_id',$event->event_id)->select('users.id', 'users.name', 'users.email')->get();

            $event['inviter'] = $inviter;
            return response()->json(['stt'=>1, 'event'=>$event]);
        }
        return response()->json(['stt'=>0,'msg'=>'No data response']);
    }

    public function createEvent(Request $request){
        $input = $request->all();
        // dd($input);
        $validator = Validator::make($request->all(), [
            'title'=>'required',
            'description'=>'required'
        ]);

        if($validator->fails()){
            return response()->json(['error'=>$validator->errors()], 401);           
        }

        $input['user_id'] = Auth::user()->id;
        try{
            DB::beginTransaction();
            $event = Event::create($input);
            //save to event_tag table
            $tags = $input['tags'];
            $this->makeTags($tags, $event->event_id);
            
            //save to location
            //Danh sach dia chi se duoc truyen qua la 1 object
            $locationArr = $input['locations'];
           // dd($locationArr);
            foreach($locationArr as $k=>$location){
                //dd($location);
                $location_data = [
                    'address'   =>$location['address'] ? $location['address'] : '',
                    'lat'   =>$location['lat'] ? $location['lat'] : '',
                    'long'   =>$location['long'] ? $location['long'] : '',
                    'event_id'  =>$event->event_id
                ];
                Location::create($location_data);
            }
            
            //save image photo table
            $imageArr = $input['images'];
            foreach($imageArr as $k=>$image){
                $image_data = [
                    'img_path'   =>$image['img_path'] ? $image['img_path'] : '',
                    'event_id'   =>$event->event_id
                ];
                Image::create($image_data);
            }
            
            //save inviter data to event_user_invite table
            $inviterArr = $input['inviter'];
            foreach($inviterArr as $k=>$inviter){
                $inviter_data = [
                    'user_id'   =>$inviter['user_id'],
                    'event_id'  =>$event->event_id
                ];
                EventUserInvite::create($inviter_data);
            }

            DB::commit();
            return response ()->json (['data' => 1,'msg'=>'Create event successfully.'],200);
        }
        catch (\Exception $e){
            Log::error($e->getMessage() . "\r\n" . $e->getTraceAsString());
            DB::rollBack();
        }
        return response ()->json (['data' => 0,'msg'=>'Create event faild.'],200);
    }

    public function updateEvent(Request $request, $eventId){
        $input = $request->all();
        $event = Event::where('event_id',$eventId)->first();

        $validator = Validator::make($request->all(), [
            'title'=>'required',
            'description'=>'required'
        ]);

        if($validator->fails()){
            return response()->json(['error'=>$validator->errors()], 401);           
        }
        try{
            DB::beginTransaction();
            $event->update($input);

            //save tag again
            $tags = $input['tags'];
            $this->makeTags($tags, $event->event_id);

            //add new locationt to this event
            $locationArr = $input['locations'];
            // dd($locationArr);
            foreach($locationArr as $k=>$location){
                //dd($location);
                $location_data = [
                    'address'   =>$location['address'] ? $location['address'] : '',
                    'lat'   =>$location['lat'] ? $location['lat'] : '',
                    'long'   =>$location['long'] ? $location['long'] : '',
                    'event_id'  =>$event->event_id
                ];
                Location::create($location_data);
            }

            //save image photo table
            $imageArr = $input['images'];
            foreach($imageArr as $k=>$image){
                $image_data = [
                    'img_path'   =>$image['img_path'] ? $image['img_path'] : '',
                    'event_id'   =>$event->event_id
                ];
                Image::create($image_data);
            }
            
            //save inviter data to event_user_invite table
            $inviterArr = $input['inviter'];
            foreach($inviterArr as $k=>$inviter){
                $inviter_data = [
                    'user_id'   =>$inviter['user_id'],
                    'event_id'  =>$event->event_id
                ];
                EventUserInvite::create($inviter_data);
            }

            DB::commit();
            return response ()->json (['data' => 1, 'msg'=>'Delete Success'],200);
        }
        catch (\Exception $e){
            Log::error($e->getMessage() . "\r\n" . $e->getTraceAsString());
            DB::rollBack();
        }
        return response ()->json (['data' => 0],200);
    }

    public function deleteEvent(Request $request, $eventId){
        //delete all data relation with this event
        try{
            DB::beginTransaction();
            //tag
            $deleteEventTag = EventTag::where('event_id',$eventId)->delete();
            Log::info("delete event Tag". $deleteEventTag);

            //location
            $deleteLocation = Location::where('event_id', $eventId)->delete();
            Log::info("delete location". $deleteLocation);

            //image
            $deleteImage = Image::where('event_id', $eventId)->delete();
            Log::info("delete image". $deleteImage);

            //delete user invited in this event
            $deleteInviter = EventUserInvite::where('event_id', $eventId)->delete();
            Log::info("delete image". $deleteInviter);

            //delete this event
            $deleteEvent = Event::where('event_id',$eventId)->delete();

            DB::commit();
            return response ()->json (['data' => 1, 'msg'=>'Delete Success'],200);
        }
        catch (\Exception $e){
            Log::error($e->getMessage() . "\r\n" . $e->getTraceAsString());
            DB::rollBack();
        }
        return response ()->json (['data' => 0],200);
    }

    public function searchEvent(Request $request){
        //ket qua chi lay thong tin cua event va tagname
        $keyword = $request->get('keyword');
        if($keyword && !empty($keyword)){
            $listEvent = Event::where('title','like', '%'.$keyword.'%')->get();

            if($listEvent){
                foreach ($listEvent as $key => $event) {
                    $tags = DB::table('tags')
                        ->join('event_tag','event_tag.tag_id','=','tags.tag_id')
                        ->join('events','events.event_id','=','event_tag.event_id')
                        ->where('events.event_id',$event->event_id)->select('tags.tag_id', 'tags.tag_name', 'tags.tag_slug')->get();

                    $listEvent[$key]['tags'] = $tags;
                }
                
                $listEvent = $listEvent->sortBy(function ($obj, $key) {
                    return strtotime($obj->start_time);
                });
                $listEvent = $listEvent->groupBy('start_date');

                return response()->json(['listevent' => $listEvent,'stt'=> 1]);
            }

        }
        return response()->json(['stt'=>0,'msg'=>'Please enter keyword']);
    }

    public function searchEventByTag(Request $request){
        //ket qua chi lay thong tin cua event va tagname
        $keyword = $request->get('tag');
        if($keyword && !empty($keyword)){
            $listEvent = DB::table('tags')
                ->join('event_tag','event_tag.tag_id','=','tags.tag_id')
                ->join('events','events.event_id','=','event_tag.event_id')
                ->where('tags.tag_name','=', $keyword)->select('events.*','tags.tag_id','tags.tag_name','tags.tag_slug')->get();

            $listEvent = $listEvent->sortBy(function ($obj, $key) {
                return strtotime($obj->start_time);
            });
            $listEvent = $listEvent->groupBy('start_date');
            return response()->json(['listevent' => $listEvent,'stt'=> 1]);
        }
        return response()->json(['stt'=>0,'msg'=>'Please enter keyword']);
    }

    function makeTags($data, $eventId){
        $eventtag = new EventTag();
        $eventtag->removeAllTags($eventId);
        if(count($data) > 0){
            for ($i=0; $i < count($data); $i++) {
                $temp = trim($data[$i]);
                $checkTag = Tag::where('tag_name','=',$temp)->first();
                if(!isset($checkTag->tag_name)){
                    $tag = Tag::create([
                        'tag_name' => trim($temp)
                    ]);

                    EventTag::create([
                        'tag_id'   =>  $tag->tag_id,
                        'event_id'  =>  $eventId
                    ]);
                }
                else{
                    EventTag::create([
                        'tag_id'   =>  $checkTag->tag_id,
                        'event_id'  =>  $eventId
                    ]);
                }
            }
        }
    }


    
}
