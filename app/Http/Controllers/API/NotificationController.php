<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notification;
use DB;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;

class NotificationController extends Controller
{
    public function getAll(Request $request){
    	$user = Auth::user();
    	$all_notifications = DB::table('notifications')
    		->where('notifications.user_id',$user->id)->select('notifications.*')->get();

	    return response()->json(['success'=>$all_notifications]);
    
    }

    public function getDetail(Request $request, $notificationId){
    	$user = Auth::user();
    	$notification = Notification::where('notification_id',$notificationId)->where('user_id', $user->id)->first();
        
        return response()->json(['success'=>$notification]);
    	
    }
}
