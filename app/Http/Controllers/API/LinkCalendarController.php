<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\CalendarLinks\Link;
use DateTime;

class LinkCalendarController extends Controller
{
    public function createCalendar(Request $request){

    	$from = DateTime::createFromFormat('Y-m-d H:i', '2018-10-04 09:00');
        $to = DateTime::createFromFormat('Y-m-d H:i', '2018-10-05 18:00');

        $link = Link::create('Sebastian\'s birthday', $from, $to)
            ->description('Cookies & cocktails!')
            ->address('my home');

        // Generate a link to create an event on Google calendar
        $result = $link->google();
        return response()->json(['data'=>$result]);
    }
}
