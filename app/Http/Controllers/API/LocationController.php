<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Location;

class LocationController extends Controller
{

	public function detailLocation(Request $request, $locationId){
		$location = Location::where('location_id',$locationId)->first();
		if(isset($location) && $location != null){
			return response()->json(['location'=>$location, 'stt'=>1]);
		}
		return response()->json(['stt'=>0, 'msg'=> 'No data response']);
	}
    public function deleteLocation(Request $request, $locationId){
    	$affect = Location::where('location_id',$locationId)->delete();
    	if($affect){
    		return response()->json(['stt'=>1,'msg'=>'Delete successfully']);
    	}
    	return response()->json(['stt'=>0, 'msg'=>'Delete Failed']);
    }
}
