<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\EventUserInvite;

class EventUserInviteController extends Controller
{
    public function deleteUserInvite(Request $request, $userId){
    	$affect = EventUserInvite::where('user_id',$userId)->delete();
    	if($affect){
    		return response()->json(['stt'=>1,'msg'=>'Delete successfully']);
    	}
    	return response()->json(['stt'=>0, 'msg'=>'Delete Failed']);
    }
}
