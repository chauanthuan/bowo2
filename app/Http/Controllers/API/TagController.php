<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Tag;
use App\EventTag;
use DB;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;

class TagController extends Controller
{
    // public function createTag(Request $request){
    // 	$user = Auth::user();
    // 	$validator = Validator::make($request->all(), [
    //         'tag_name'=>'required',
    //     ]);

    //     if($validator->fails()){
    //         return response()->json(['error'=>$validator->errors()], 401);           
    //     }
    //     $temp = trim($request->get('tag_name'));
    //     $checkTag = Tag::where('tag_name','=',$temp)->first();
    //     if(!isset($checkTag->tag_name)){
    //         $tag = Tag::create([
    //             'tag_name' => trim($temp),
    //             'tag_slug' => str_slug(trim($temp))
    //         ]);

    //         EventTag::create([
    //             'tag_id'   =>  $tag->tag_id,
    //             'user_id'  =>  $user->id
    //         ]);
    //     }
    //     else{
    //         EventTag::create([
    //             'tag_id'   =>  $checkTag->tag_id,
    //             'user_id'  =>  $user->id
    //         ]);
    //     }

    //     return response()->json(['stt'=>1,'msg'=>'Create tag successfully.']);
    // }

    public function getAll(Request $request){
        $user = Auth::user();
        $all_tags = DB::table('tags')
            ->join('event_tag','event_tag.tag_id','=','tags.tag_id')
            ->join('events','events.event_id','=','event_tag.event_id')
            ->where('events.user_id',$user->id)->select('tags.*',DB::raw('count(event_tag.tag_id) as event_count'))->groupBy('event_tag.tag_id')->get();
        
        return response()->json(['success'=>$all_tags]);
    }

    public function detailTag(Request $request, $id){
        $tag = Tag::where('tag_id',$id)->first();
        return response()->json(['success'=>$tag]);
    }   
    public function updateTag(Request $request, $id)
    {
    	$validator = Validator::make($request->all(), [
            'tag_name'=>'required',
        ]);
        
        if($validator->fails()){
            return response()->json(['error'=>$validator->errors()], 401);           
        }

        $tag = Tag::where('tag_id',$id)->first();
        if(!$tag){
            return response()->json(['stt'=>0, 'msg'=>'No data response']);
        }
        
    	$tag->update([
    		'tag_name' => trim($request->get('tag_name')),
            'tag_slug' => str_slug(trim(trim($request->get('tag_name'))))
    	]);
    	return response()->json(['stt'=>1,'msg'=>'successfully']);
    }

    public function deleteTag(Request $request,$id){
        try{
            DB::beginTransaction();
            //delete tat ca nhung tag co tag_id = $id trong bang event tag truoc
            $deleteEventTag = EventTag::where('tag_id',$id)->delete(); 

            //delet in Tag table
        	$affect = Tag::where('tag_id',$id)->delete();
        	
            DB::commit();
    		return response()->json(['stt'=>1,'msg'=>'Delete successfully']);
        }
        catch (\Exception $e){
            Log::error($e->getMessage() . "\r\n" . $e->getTraceAsString());
            DB::rollBack();
        }
        return response ()->json (['data' => 0, 'msg'=>'Delete faild'],200);
    }
}
