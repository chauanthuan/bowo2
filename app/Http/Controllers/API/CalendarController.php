<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Calendar;
use DB;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;

class CalendarController extends Controller
{
   	public function getAll(Request $request){
    	$user = Auth::user();
    	$all_calendar = DB::table('calendar')
    		->where('calendar.user_id',$user->id)->select('calendar.*')->get();

	    return response()->json(['success'=>$all_calendar]);
    
    }

    public function getDetail(Request $request, $calendarId){
    	$user = Auth::user();
    	$calendar = Calendar::where('calendar_id',$calendarId)->where('user_id', $user->id)->first();
        
        return response()->json(['success'=>$calendar]);
    	
    }

    public function createCalendar(Request $request){
    	$user = Auth::user();
    	// dd($user->id);
    	$validator = Validator::make($request->all(), [
            'calendar_name'=>'required',
            'email'=>'required|email',
            'calendar_type'=>'required'
        ]);

        if($validator->fails()){
            return response()->json(['error'=>$validator->errors()], 401);           
        }
       	$input = $request->all();
       	$input['user_id'] = $user->id;
        $calendar = Calendar::create($input);
        if($calendar){
        	return response()->json(['stt'=>1,'msg'=>'Create calendar successfully.']);
        }
        return response()->json(['stt'=>0,'msg'=>'Create calendar failed.']);
    }

    public function updateCalendar(Request $request, $calendarId){
    	$validator = Validator::make($request->all(), [
            'calendar_name'=>'required',
            'email'=>'required',
            'calendar_type'=>'required'
        ]);
        
        if($validator->fails()){
            return response()->json(['error'=>$validator->errors()], 401);           
        }

        $calendar = calendar::where('calendar_id',$calendarId)->first();
        if(!$calendar){
            return response()->json(['stt'=>0, 'msg'=>'No data response']);
        }
        
    	$calendar->update([
    		'calendar_name' 	=> trim($request->get('calendar_name')),
            'calendar_type' 	=> $request->get('calendar_type'),
            'email'				=>$request->get('email')
    	]);
    	return response()->json(['stt'=>1,'msg'=>'successfully']);
    }

    public function deleteCalendar(Request $request,$id){
            //delet in Tag table
        $affect = Calendar::where('calendar_id',$id)->delete();
        if($affect){
    		return response()->json(['stt'=>1,'msg'=>'Delete successfully']);
        }
        return response ()->json (['data' => 0, 'msg'=>'Delete faild'],200);
    }
}
