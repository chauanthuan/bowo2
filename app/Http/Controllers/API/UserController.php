<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use Hash;
use DB;

class UserController extends Controller
{
    public $successStatus = 200;
    public $user;

    public function __construct(User $user){
        $this->user = $user;
    }
    /*
    * Login Api
    * Return response
    */
    public function login(Request $request){
    	if(Auth::attempt(['email'=>request('email'), 'password'=>request('password'), 'account_type'=> User::ACCOUNT_TYPE_EMAIL])){
    		$user = Auth::user();
    		$success['token'] =  $user->createToken('MyApp')->accessToken;
            $success['user'] = $user;
    		return response()->json(['success'=>$success], $this->successStatus);
    	}
    	else{
    		return response()->json(['error'=>'Unanthorised'], 401);
    	}
    }

    /*
    * Register Api
    *
    */

    public function register(Request $request){
    	$validator = Validator::make($request->all(),[
    		'name'=> 'required',
    		'email'=>'required|email|unique:users,email',
    		'password'=>'required',
    		'password_confirmation'=>'required|same:password'
    	]);

    	if($validator->fails()){
    		return response()->json(['error'=>$validator->errors()], 401);           
    	}

    	$input = $request->all();
    	$input['password'] = bcrypt($input['password']);
        $input['account_type'] = User::ACCOUNT_TYPE_EMAIL;
    	$user = User::create($input);
    	$success['token'] = $user->createToken('MyApp')->accessToken;
    	$success['name'] = $user->name;
    	return response()->json(['success'=>$success], $this->successStatus);
    }

    /*
	* Detail Api
	*/
	public function details(){
		$user = Auth::user();
		return response()->json(['success'=>$user], $this->successStatus);
	}

    /*
    * Detail Api
    */
    public function getdetail(){
        $user = Auth::user();
        return response()->json(['success'=>$user], $this->successStatus);
    }

    /*
    * Create image from base 64
    */
    public function updateAvatar(Request $request){ 
        $validator = Validator::make($request->all(),[
            'avatar'=> 'required'
        ]);
        if($validator->fails()){
            return response()->json(['error'=>$validator->errors()], 401);           
        }

        $image = $request->input('avatar');  // your base64 encoded
        if ( base64_encode(base64_decode($image, true)) != $image){
            return response()->json(['error'=>'Avatar base64 format is invalid.'], 401);
        }

        $image = str_replace('data:image/png;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $imageName = 'image_'.str_random(10).'.'.'png';
        $path = '/avatar/' . $imageName;
        if (!file_exists(public_path().'/avatar')) {
            mkdir(public_path().'/avatar', 0777, true);
        }
        // $success = \Storage::disk('public')->put($path,base64_decode($image));
        $success = file_put_contents(public_path().$path, base64_decode($image));
        if($success){
            $this->user->where('id',Auth::user()->id)->update(['avatar'=>$path]);
            return response()->json(['stt'=>1,'msg'=>'Update Avatar successfully']);
        }
        return response()->json(['stt'=>0,'msg'=>'Upload avatar failed']);
    }
    /*
    * Forgot password Api
    */

    public function forgotPassword(Request $request){
        $validator = Validator::make($request->all(),[
            'email'=> 'required | email'
        ]);

        if($validator->fails()){
            return response()->json(['error'=>$validator->errors()], 401);           
        }
        //send mail to reset password
        return response()->json(['success'=>'Send mail reset password success'], $this->successStatus);
    }

    public function resetPassword(Request $request){
        //sẽ có email truyền thêm qua
        $validator = Validator::make($request->all(), [
            'password'=>'required',
            'password_confirmation'=>'required|same:password'
        ]);
        if($validator->fails()){
            return response()->json(['error'=>$validator->errors()], 401);           
        }

        $newpass = bcrypt($request->get('password'));
        $email = $request->get('email');
        //update password
        $user = User::where('email',$email)->update(['password'=>$newpass]);

        if($user){
           return response()->json(['success'=>'Update password successfully.'], $this->successStatus);
        }
    }

    public function changepassword(Request $request){
        $user  = Auth::user();
        if(!$user){
            return response()->json(['error'=>'Unanthorisedssss', 'stt'=>0,'msg'=>'Unanthorisedaa']);
        }
        $validator = Validator::make($request->all(), [
            'old_password'=>'required',
            'password'=>'required',
            'password_confirmation'=>'required|same:password'
        ]);

        if($validator->fails()){
            return response()->json(['error'=>$validator->errors()], 401);           
        }
        if(!Hash::check($request->get('old_password'), $user->password)){
            return response()->json(['error'=>'Old password not right.', 'stt'=>0]); 
        }

        $newpass = bcrypt($request->get('password'));
        $affect = $user->update(['password'=>$newpass]);
        
        if($affect){
            return response()->json(['success'=>'Update password successfully.'], $this->successStatus);
        }
    }

    public function logout() {
        $accessToken = Auth::user()->token();
        DB::table('oauth_refresh_tokens')
            ->where('access_token_id', $accessToken->id)
            ->update([
                'revoked' => true
            ]);

        $accessToken->revoke();
        return response()->json(null, 204);
    }



    public function postRegisterSocial(Request $request) {
        $success = [];
        $input = $request->all();
        $validator = \Validator::make($request->all(), [
          'token'        => 'required',
          'account_type' => 'required'
        ]);

        if(count($validator->errors())){
            return response()->json(['error'=>$validator->errors()], 401);
            // return response()->json(['error'=>'Unanthorised'], 401);
        } else {
          $item = [];
          if($input['account_type'] == User::ACCOUNT_TYPE_FACEBOOK) {
            $url = "https://graph.facebook.com/me?fields=id,name,email,first_name,last_name,gender&access_token=".$input['token'];
            $customer = $this->getRequest($url);
            if(!empty($customer) && !isset($customer->error) ) {
              $item = [
                'name'       => $this->getVal($customer , 'name'),
                'email'       => $this->getVal($customer , 'email'),
                'account_type' => User::ACCOUNT_TYPE_FACEBOOK,
                'social_id'   => $this->getVal($customer , 'id'),
                'gender'      => $this->getVal($customer , 'gender') == 'male' ? User::GENDERMALE : User::GENDERFEMALE,
                'lang'        => $this->getVal($input , 'lang', 'vi'),
                'state'       => User::APPROVED,
              ];
              //$item['avatar'] = 'http://graph.facebook.com/'.$item['social_id'].'/picture?type=large';

            } else {
                return response()->json(['error'=>$customer->error], 401);
            }
          } else if($input['account_type'] == User::ACCOUNT_TYPE_GOOGLE) {
            $url = 'https://www.googleapis.com/plus/v1/people/me?access_token='.$input['token'];
            $customer = $this->getRequest($url);
            if(!empty($customer) && !isset($customer->error)) {
              $email=  $customer->emails[0]->value;
              //$image = $customer->image->url;
              $item = [
                'name'       => $this->getVal($customer , 'displayName'),
                'email'       => $email,
                'account_type' => User::ACCOUNT_TYPE_GOOGLE,
                'social_id'   => $this->getVal($customer , 'id'),
                'lang'        => $this->getVal($customer , 'lang', 'vi'),
                'state'       => User::APPROVED,
              //  'avatar'      => str_replace('?sz=50', '', $image)
              ];
            } else {
               return response()->json(['error'=>$customer->error], 401);
            }
          } else if($input['account_type'] == User::ACCOUNT_TYPE_GITHUB) {
            $url = 'https://api.github.com/user?access_token='.$input['token'];
            $customer = $this->getRequest($url);
            if(!empty($customer) && !isset($customer->error)) {
              $email=  $customer->email;
              //$image = $customer->image->url;
              $item = [
                'name'       => $this->getVal($customer , 'name'),
                'email'       => $email,
                'account_type' => User::ACCOUNT_TYPE_GITHUB,
                'social_id'   => $this->getVal($customer , 'id'),
                'lang'        => $this->getVal($customer , 'lang', 'vi'),
                'state'       => User::APPROVED,
              //  'avatar'      => str_replace('?sz=50', '', $image)
              ];
            } else {
               return response()->json(['error'=>$customer->error], 401);
            }
          }

          if(!empty($item)) {

            $customer = $this->user->where('social_id' ,'=' , $item['social_id'])
                                       ->where('account_type' ,'=' , $item['account_type'])
                                       ->first();
            if(empty($customer)) {
              $customer = $this->user->create($item);
            } 


            $success['token'] = $customer->createToken('MyApp')->accessToken;
            $success['name']  = $customer->name;
            
          }

        }
        return response()->json(['success'=>$success ], $this->successStatus);
    }

    public function getRequest($url) {
        $client = new \GuzzleHttp\Client(['verify' =>false, 'http_errors' => false]);
        $requestGuzzle  = $client->request('GET', $url, []);
        $response = $requestGuzzle->getBody()->getContents();
        return $this->jsonDecode($response);
    }

    public  function getVal($item , $key , $default = '') {
        $item = (array)$item;
        return isset($item[$key]) && !empty($item[$key]) ? $item[$key] : $default;
    }

    public  function jsonDecode($arr, $isArray = false) {
        return !empty($arr) ? json_decode($arr , $isArray) : $arr;
    }

    public function periodTracker(Request $request)
    {
        $validator = \Validator::make($request->all(), [
          'last_period'        => 'required|date|before_or_equal:today',
        ]);
        if($validator->fails()){
            return response()->json(['error'=>$validator->errors()], 401);           
        }

        $last_period = $request->get('last_period');
        $longtime_period = $request->get('longtime_period',5);
        $cycle_period = $request->get('cycle_period', 28);
        $user = Auth::user();
        $effect = $this->user->where('id',$user->id)->update(['last_period'=>$last_period,'longtime_period'=>$longtime_period,'cycle_period'=>$cycle_period]);
        if($effect){
           return response()->json(['stt'=>1, 'msg'=>'Set period tracker successfully']); 
        }
        return response()->json(['stt'=>0, 'msg'=>'Set period tracker failed']);
    }
}
