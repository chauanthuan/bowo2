<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Image;
class ImageController extends Controller
{
    public function deleteImage(Request $request, $imageId){
    	$affect = Image::where('img_id',$imageId)->delete();
    	if($affect){
    		return response()->json(['stt'=>1,'msg'=>'Delete successfully']);
    	}
    	return response()->json(['stt'=>0, 'msg'=>'Delete Failed']);
    }
}
