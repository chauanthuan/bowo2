<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Calendar extends Model
{
	const CALENDAR_TYPE_DEVICE 	= 1;
	const CALENDAR_TYPE_GOOGLE 	= 2;
	const CALENDAR_TYPE_IOS 	= 3;

    protected $primaryKey = 'calendar_id';

    protected $table = 'calendar';

    protected $fillable = [
        'calendar_name', 'calendar_type','email','user_id'
    ];
}
