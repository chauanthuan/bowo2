<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{

	const NOTIFICATION_TYPE_EMAIL             = 1;
    const NOTIFICATION_TYPE_GIFT           	  = 2;

    const NOTIFICATION_STATE_NOTREAD          = 0;
    const NOTIFICATION_STATE_SEEN             = 1;

    protected $primaryKey = 'notification_id';

    protected $table = 'notifications';

    protected $fillable = [
        'title', 'description', 'type','state', 'user_id'
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }
}
