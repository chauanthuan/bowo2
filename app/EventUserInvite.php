<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventUserInvite extends Model
{
    protected $table = 'event_user_invite';
    
    protected $fillable = [
        'event_id','user_id'
    ];

    public function event(){
        return $this->belongsTo('App\Event');
    }

    public function user(){
        return $this->hasMany('App\User');
    }
}
