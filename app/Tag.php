<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
	protected $primaryKey = 'tag_id';

    protected $table = 'tags';

    protected $fillable = [
        'tag_name','tag_slug'
    ];

    public static function eventTag(){
        return $this->belongsTo('App\EventTag');
    }
}
