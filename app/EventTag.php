<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class EventTag extends Model
{
    protected $table = 'event_tag';
    
    protected $fillable = [
        'event_id','tag_id'
    ];

    public function event(){
        return $this->belongsTo('App\Event');
    }

    public function tags(){
        return $this->hasMany('App\Tag');
    }

    public function removeAllTags($eventId){
        $result = \DB::table('event_tag')
            ->where('event_tag.event_id','=',$eventId);
        return $result->delete();
    }
}
