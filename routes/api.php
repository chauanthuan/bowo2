<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'API\UserController@login');
Route::post('register','API\UserController@register');

Route::post('/customer/rgsocial',['uses'=>'API\UserController@postRegisterSocial'])->name('customer.rgsocial');

Route::group(['middleware'=>'auth:api','namespace' => 'API'], function(){
	Route::post('details','UserController@details');
	Route::post('period-tracker','UserController@periodTracker');
	Route::get('user/detail','UserController@getdetail');
	Route::post('user/update-avatar','UserController@updateAvatar');
	Route::post('changepassword','UserController@changepassword');
	Route::get('logout', 'UserController@logout');

	Route::prefix('calendar')->group(function(){
		Route::get('/','CalendarController@getAll');
		Route::post('/', 'CalendarController@createCalendar');
		Route::get('/{id}','CalendarController@getDetail');
		Route::put('/{id}','CalendarController@updateCalendar');
		Route::delete('/{id}', 'CalendarController@deleteCalendar');
	});
	Route::prefix('notification')->group(function(){
		Route::get('/','NotificationController@getAll');
		Route::get('/{id}','NotificationController@getDetail');
	});
	Route::prefix('tag')->group(function () {
	    Route::get('/', 'TagController@getAll');
		Route::post('/', 'TagController@createTag');
		Route::get('/{id}', 'TagController@detailTag');
		Route::put('/{id}', 'TagController@updateTag');
		Route::delete('/{id}', 'TagController@deleteTag');
	});
	Route::prefix('event')->group(function () {
	    Route::get('/', 'EventController@getAll');
		Route::post('/', 'EventController@createEvent');
		Route::get('/search','EventController@searchEvent');
		Route::get('/searchbytag','EventController@searchEventByTag');
		Route::get('/{id}', 'EventController@detailEvent');
		Route::put('/{id}', 'EventController@updateEvent');
		Route::delete('/{id}', 'EventController@deleteEvent');

	});

	Route::prefix('location')->group(function () {
		Route::delete('/{id}', 'LocationController@deletelocation');
		Route::put('/{id}', 'LocationController@updateLocation');
	});

	Route::prefix('image')->group(function () {
		Route::delete('/{id}', 'ImageController@deleteImage');
	});
});